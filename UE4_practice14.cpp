﻿#include <iostream>

int main()
{
    std::string MyString("Example of string");
    std::cout << "String:" << MyString << "\n";
    std::cout << "Length:" << MyString.length() << "\n";
    std::cout << "First char:" << MyString.substr(0, 1) << "\n";
    std::cout << "Last char:" << MyString.substr(MyString.length()-1, 1) << "\n";
}

